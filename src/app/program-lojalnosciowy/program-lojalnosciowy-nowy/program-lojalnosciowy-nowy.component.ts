import {Component, OnInit} from '@angular/core';
import {ProgramLojalnosciowyService} from "../program-lojalnosciowy.service";
import {Router} from "@angular/router";
import {CennikService} from "../../cennik/cennik.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-program-lojalnosciowy-nowy',
  templateUrl: './program-lojalnosciowy-nowy.component.html',
  styleUrls: ['./program-lojalnosciowy-nowy.component.css']
})
export class ProgramLojalnosciowyNowyComponent implements OnInit {
  ile = 10;

  constructor(public programLojanoscioyService: ProgramLojalnosciowyService, public cennikService: CennikService) {
  }

  ngOnInit(): void {
  }


  iloscPopraw(ilosc: number) {
    if (ilosc < 1) {
      this.ile = 1;
    }
  }

  dodaj(form: NgForm) {
    this.programLojanoscioyService.dodaj('dodaj', form.value.produkt, form.value.ilosc);
  }
}
