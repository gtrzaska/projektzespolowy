import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProgramLojalnosciowyNowyComponent} from './program-lojalnosciowy-nowy.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";

describe('ProgramLojalnosciowyNowyComponent', () => {
  let component: ProgramLojalnosciowyNowyComponent;
  let fixture: ComponentFixture<ProgramLojalnosciowyNowyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgramLojalnosciowyNowyComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramLojalnosciowyNowyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
