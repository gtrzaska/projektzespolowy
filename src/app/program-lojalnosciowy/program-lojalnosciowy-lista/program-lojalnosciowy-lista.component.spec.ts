import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProgramLojalnosciowyListaComponent} from './program-lojalnosciowy-lista.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";

describe('ProgramLojalnosciowyListaComponent', () => {
  let component: ProgramLojalnosciowyListaComponent;
  let fixture: ComponentFixture<ProgramLojalnosciowyListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgramLojalnosciowyListaComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramLojalnosciowyListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
