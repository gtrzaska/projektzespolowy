import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PromocjaComponent} from './promocja.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";

describe('PromocjaComponent', () => {
  let component: PromocjaComponent;
  let fixture: ComponentFixture<PromocjaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PromocjaComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromocjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
