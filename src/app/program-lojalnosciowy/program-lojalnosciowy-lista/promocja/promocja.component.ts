import {Component, Input, OnInit} from '@angular/core';
import {ProgramLojalnosciowyService} from "../../program-lojalnosciowy.service";
import swal from "sweetalert";
import {ProfilService} from "../../../profil/profil.service";

@Component({
  selector: 'app-promocja',
  templateUrl: './promocja.component.html',
  styleUrls: ['./promocja.component.css']
})
export class PromocjaComponent implements OnInit {
  @Input() promocja = [0, '', 0];
  @Input() id: number;
  userData: {
    email: string,
    uprawnienia: string,
    imie: string,
    nazwisko: string
  };

  constructor(private programLojanoscioyService: ProgramLojalnosciowyService, public profilService: ProfilService) {
  }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('userData'));
  }

  usun(id) {
    swal({
      title: "Czy na pewno?",
      icon: "warning",
      buttons: ["Nie", "Tak"],

    })
      .then(potwierdz => {
        if (potwierdz) {
          swal("Sukces!", "Usunięto nagrodę!", "success");
          this.programLojanoscioyService.usun('usun', +id);
        }
      });
  }

  odbierz(id) {
    swal({
      title: "Czy na pewno?",
      text: "Nie będziesz mógł odzyskać punktów",
      icon: "warning",
      buttons: ["Nie", "Tak"],
      // dangerMode: true,

    })
      .then(potwierdz => {
        if (potwierdz) {
          swal("Sukces!", "Odebrano nagrodę!", "success");
          this.profilService.user[11] -= +this.promocja[2];
          this.programLojanoscioyService.odbierz('odbierz', +id, +this.promocja[2], this.userData.email, this.userData.imie, this.promocja[1].toString());
        }
      });
    // this.programLojanoscioyService.odbierz('usun',+id);
  }
}
