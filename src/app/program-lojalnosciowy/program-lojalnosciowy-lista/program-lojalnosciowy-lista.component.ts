import {Component, OnInit} from '@angular/core';
import {ProgramLojalnosciowyService} from "../program-lojalnosciowy.service";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-program-lojalnosciowy-lista',
  templateUrl: './program-lojalnosciowy-lista.component.html',
  styleUrls: ['./program-lojalnosciowy-lista.component.css']
})
export class ProgramLojalnosciowyListaComponent implements OnInit {
  email: string;
  uprawnienia: number;
  isAuth = false;

  constructor(public programLojalnosciowyService: ProgramLojalnosciowyService, private authService: AuthService) {
    this.authService.user.subscribe(user => {
      this.isAuth = !user ? false : true;
      if (this.isAuth) {
        this.email = user.email;
        this.uprawnienia = +user.uprawnienia;
      }
    });
  }

  ngOnInit(): void {
    this.programLojalnosciowyService.kod = '';
  }

}
