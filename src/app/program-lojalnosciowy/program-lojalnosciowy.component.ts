import {Component, OnInit} from '@angular/core';
import {ProfilService} from "../profil/profil.service";
import {ProgramLojalnosciowyService} from "./program-lojalnosciowy.service";

@Component({
  selector: 'app-program-lojalnosciowy',
  templateUrl: './program-lojalnosciowy.component.html',
  styleUrls: ['./program-lojalnosciowy.component.css']
})
export class ProgramLojalnosciowyComponent implements OnInit {
  userData: {
    email: string,
    uprawnienia: string,
    imie: string,
    nazwisko: string
  };

  constructor(public profilService: ProfilService, public programLojanoscioyService: ProgramLojalnosciowyService) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('userData') != null) {
      this.userData = JSON.parse(localStorage.getItem('userData'));
    } else {
      this.userData.email = 'email';
      this.userData.uprawnienia = '1';
      this.userData.imie = 'imie';
      this.userData.nazwisko = 'nazwisko';
    }
    this.profilService.profil(this.userData.email, +this.userData.uprawnienia);
  }

}
