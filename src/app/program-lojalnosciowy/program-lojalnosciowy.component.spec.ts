import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientModule} from "@angular/common/http";

import {ProgramLojalnosciowyComponent} from './program-lojalnosciowy.component';
import {RouterTestingModule} from "@angular/router/testing";
import {FormsModule} from "@angular/forms";

describe('ProgramLojalnosciowyComponent', () => {
  let component: ProgramLojalnosciowyComponent;
  let fixture: ComponentFixture<ProgramLojalnosciowyComponent>;
  let userData: {
    email: 'email',
    uprawnienia: '1',
    imie: 'imie',
    nazwisko: 'nazwisko'
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgramLojalnosciowyComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramLojalnosciowyComponent);
    component = fixture.componentInstance;
    component.userData = userData;
    fixture.detectChanges();
  });

/*  it('should create', () => {
    expect(component).toBeTrue();
  });*/
});
