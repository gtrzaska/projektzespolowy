import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import swal from "sweetalert";

@Injectable({
  providedIn: 'root'
})
export class ProgramLojalnosciowyService implements OnInit {
  data = [];
  kod: string = '';
  promocje_array = [];
  isLoading = false;
  link = 'https://server439815.nazwa.pl/PZesp/';

  constructor(private http: HttpClient, private router: Router) {
    this.promocje();
  }

  ngOnInit() {
    this.kod = '';
  }


  promocje() {
    this.isLoading = true;
    this.promocje_array = [];
    this.data = [];
    this.http.get(this.link + 'nagrody_fetch.php').subscribe(data => {
      this.data.push(data);
      for (let i = 0; i < this.data[0].length; i++) {
        this.promocje_array.push([this.data[0][i].id, this.data[0][i].Nazwa, +this.data[0][i].Punkty]);
      }
      this.isLoading = false;
    }, error => {
    });
  }

  dodaj(tryb: string, produkt: string, ilosc: number) {
    this.isLoading = true;
    this.http.post(this.link + 'nagrody.php', {tryb, produkt, ilosc}).subscribe(error => {
      this.isLoading = false;
      if (error == '200') {
        this.promocje();
        this.router.navigate(['./program-lojalnosciowy']);
        this.isLoading = false;
      } else {
        swal("Błąd!");
        this.isLoading = false;
      }
    }, error => {

    });
  }

  usun(tryb: string, id: number) {
    this.isLoading = true;
    this.http.post(this.link + 'nagrody.php', {tryb, id}).subscribe(error => {
      this.isLoading = false;
      if (error == '200') {
        this.promocje();
        this.router.navigate(['./program-lojalnosciowy']);
        this.isLoading = false;

      } else {
        swal("Błąd!");
        this.isLoading = false;
      }
    }, error => {

    });
  }

  odbierz(tryb: string, id: number, punkty: number, mail: string, imie: string, produkt: string) {
    this.kod = this.generatorKodu(5).toUpperCase();
    let kodPromocja = this.kod;
    this.isLoading = true;
    this.http.post(this.link + 'nagrody.php', {tryb, produkt, id, kodPromocja, mail, imie, punkty}).subscribe(error => {
      this.isLoading = false;
      if (error == '200') {
        this.promocje();
        this.isLoading = false;

      } else {
        swal("Błąd!");
        this.isLoading = false;
      }
    }, error => {

    });

  }

  generatorKodu(length: number) {
    let ret = "";
    while (ret.length < length) {
      ret += Math.random().toString(16).substring(2);
    }
    Math.floor(Math.random() * 10);

    return ret.substring(0, length);
  }

}
