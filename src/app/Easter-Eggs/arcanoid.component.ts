import {Component, Injectable, OnInit} from '@angular/core';
import {ArcanoidService} from "./arcanoid.service";
import swal from "sweetalert";

@Component({
  selector: 'ur-arcanoid',
  templateUrl: './arcanoid.component.html',
})

@Injectable({
  providedIn: 'root',
})

export class ArcanoidComponent implements OnInit {
  screenWidth = window.innerWidth;

  constructor(private arcanoidService: ArcanoidService) {
    this.screenWidth = window.innerWidth;
  }

  ngOnInit() {
    var xd = 1;

    document.getElementById("div1").style.cursor = "none";
    var p = document.getElementById("klepki");
    p.style.position = "relative";

    for (let i = 0; i < 4; i++)
      for (let j = 0; j < 4; j++) {

        var b = document.createElement("div");
        b.style.backgroundColor = "red";
        b.style.width = "120px";
        b.style.height = "25px";
        b.id = "klepka" + xd;
        b.style.marginTop = 31 * i + "px";
        b.style.marginLeft = 126 * j + "px";
        b.style.border = "solid black 1px ";
        b.style.position = "absolute";
        p.appendChild(b);
        xd++
      }

    this.gra();
  }

  gra() {
    let ruchValue = 2;
    let pkt = 0;
    let poziom = 215;
    let pion = 35;
    let poz = true;
    var pio = true;

    let internal = setInterval(() => {
      var rect = document.getElementById("div2").getBoundingClientRect();
      var pi = document.getElementById("pilka");

      pi.style.bottom = pion + "px";
      pi.style.left = poziom + "px";


      var lel = rect.left - 36 - (window.innerWidth / 2 - 285);
      var lol = lel + 70;
      var polpal = lel + 35;
      if (poziom >= 490)
        poz = false;
      else if (poziom <= 0)
        poz = true;
      if (pion >= 490)
        pio = false;
      else if (pion <= 0) {
        ruchValue = 0;
        pion = 50;
        clearInterval(internal);
        this.arcanoidService.koniec = true;
        swal("PORAŻKA!");

      }
      if (pion <= 31 && poziom >= lel && poziom <= polpal) {
        pio = true;
        poz = false;
      } else if (pion <= 31 && pion >= 24 && poziom > polpal && poziom <= lol) {
        pio = true;
        poz = true;
      }


      if (document.getElementById("klepka16") != null) {
        if (pion >= 380 && pion <= 405 && poziom >= 380 && poziom <= 500) {
          pio = false;
          pkt++;
          document.getElementById("klepka16").remove()
        }
      }
      if (document.getElementById("klepka15") != null) {
        var klepka = document.getElementById("klepka15").getBoundingClientRect();
        if (pion >= 380 && pion <= 405 && poziom >= 255 && poziom <= 375) {
          pio = false;
          pkt++;
          document.getElementById("klepka15").remove()
        }
      }
      if (document.getElementById("klepka14") != null) {
        var klepka = document.getElementById("klepka14").getBoundingClientRect();
        if (pion >= 380 && pion <= 405 && poziom >= 125 && poziom <= 250) {
          pio = false;
          pkt++;
          document.getElementById("klepka14").remove()
        }
      }
      if (document.getElementById("klepka13") != null) {
        if (pion >= 380 && pion <= 405 && poziom >= 0 && poziom <= 122) {
          pio = false;
          pkt++;
          document.getElementById("klepka13").remove()
        }
      }
      if (document.getElementById("klepka12") != null) {
        if (pion >= 410 && pion <= 435 && poziom >= 380 && poziom <= 500) {
          pio = false;
          pkt++;
          document.getElementById("klepka12").remove()
        }
      }
      if (document.getElementById("klepka11") != null) {
        if (pion >= 410 && pion <= 435 && poziom >= 255 && poziom <= 375) {
          pio = false;
          pkt++;
          document.getElementById("klepka11").remove()
        }
      }
      if (document.getElementById("klepka10") != null) {
        if (pion >= 410 && pion <= 435 && poziom >= 125 && poziom <= 250) {
          pio = false;
          pkt++;
          document.getElementById("klepka10").remove()
        }
      }
      if (document.getElementById("klepka9") != null) {
        if (pion >= 410 && pion <= 435 && poziom >= 0 && poziom <= 122) {
          pio = false;
          pkt++;
          document.getElementById("klepka9").remove()
        }
      }
      if (document.getElementById("klepka8") != null) {
        if (pion >= 440 && pion <= 470 && poziom >= 380 && poziom <= 500) {
          pio = false;
          pkt++;
          document.getElementById("klepka8").remove()
        }
      }
      if (document.getElementById("klepka7") != null) {
        if (pion >= 440 && pion <= 470 && poziom >= 255 && poziom <= 375) {
          pio = false;
          pkt++;
          document.getElementById("klepka7").remove()
        }
      }
      if (document.getElementById("klepka6") != null) {

        if (pion >= 440 && pion <= 470 && poziom >= 125 && poziom <= 250) {
          pio = false;
          pkt++;
          document.getElementById("klepka6").remove()
        }
      }
      if (document.getElementById("klepka5") != null) {
        if (pion >= 440 && pion <= 470 && poziom >= 0 && poziom <= 122) {
          pio = false
          pkt++
          document.getElementById("klepka5").remove()
        }
      }
      if (document.getElementById("klepka4") != null) {
        if (pion >= 475 && pion <= 500 && poziom >= 380 && poziom <= 500) {
          pio = false;
          pkt++;
          document.getElementById("klepka4").remove()
        }
      }
      if (document.getElementById("klepka3") != null) {
        if (pion >= 475 && pion <= 500 && poziom >= 255 && poziom <= 375) {
          pio = false;
          pkt++;
          document.getElementById("klepka3").remove()
        }
      }
      if (document.getElementById("klepka2") != null) {
        if (pion >= 475 && pion <= 500 && poziom >= 125 && poziom <= 250) {
          pio = false;
          pkt++;
          document.getElementById("klepka2").remove()
        }
      }
      if (document.getElementById("klepka1") != null) {
        if (pion >= 475 && pion <= 500 && poziom >= 0 && poziom <= 122) {
          pio = false;
          pkt++;
          document.getElementById("klepka1").remove()
        }
      }
      if (pkt === 16) {
        ruchValue = 0;
        this.arcanoidService.koniec = true;
        pi.remove();
        ruchValue = 0;
        pion = 50;
        clearInterval(internal);
        swal("WYGRANA");
      }

      if (pio)
        pion += ruchValue;
      else if (!pio)
        pion -= ruchValue;
      if (poz)
        poziom += ruchValue;
      else if (!poz)
        poziom -= ruchValue;
    }, 10);
  }

  ruch(event) {
    var rect = document.getElementById("div1").getBoundingClientRect();

    let pa = document.getElementById("div2");
    var x = event.clientX - (this.screenWidth / 2 - 250);

    if (x > 430) {
      x = 430;
    } else if (x < 0) {
      x = 0;
    }
    pa.style.left = x + "px"
  }

//36

}
