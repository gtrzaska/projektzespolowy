import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AuthService} from './auth/auth.service';
import {HttpClient} from "@angular/common/http";
import {ArcanoidComponent} from "./Easter-Eggs/arcanoid.component";
import {ArcanoidService} from "./Easter-Eggs/arcanoid.service";
import {animate, state, style, transition, trigger} from "@angular/animations";

export type FadeState = 'visible' | 'hidden';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0}),
        animate(400, style({opacity: 1}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(400, style({opacity: 0}))
      ])
    ])
  ]
})
export class AppComponent implements OnInit {
  data = [];
  wasArcanoid: boolean = false;

  constructor(private authService: AuthService, private http: HttpClient, public arcanoidComponent: ArcanoidComponent, public arcanoidService: ArcanoidService) {
  }


  easterEgg: string = '';

  ngOnInit() {


    this.authService.autoLogin();

    window.addEventListener('keydown', e => {
      handleEvent(e); // e is KeyboardEvent
    });

    let handleEvent = (event: KeyboardEvent) => {
      const arcanoid = 'arc123';
      const {key} = event;
      this.easterEgg += key;
      if (this.easterEgg === arcanoid) {
        this.arcanoidService.koniec = false;
        this.easterEgg = '';
        this.wasArcanoid = true;
      }

      for (let i = 0; i < this.easterEgg.length; i++) {
        if (arcanoid.charAt(i) != this.easterEgg.charAt(i)) {
          this.easterEgg = '';
        }
      }
    }
  }

  state: FadeState;
  // tslint:disable-next-line: variable-name
  private _show: boolean;
  get show() {
    return this._show;
  }

  @Input()
  set show(value: boolean) {
    if (value) {
      // show the content and set it's state to trigger fade in animation
      this._show = value;
      this.state = 'visible';
    } else {
      // just trigger the fade out animation
      this.state = 'hidden';
    }
  }


}
