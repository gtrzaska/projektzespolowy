import {Component, OnInit} from '@angular/core';
import {HistoriaService} from "./historia.service";
import {style, state, animate, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-historia',
  templateUrl: './historia.component.html',
  styleUrls: ['./historia.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0}),
        animate(130, style({opacity: 1}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(130, style({opacity: 0}))
      ])
    ])
  ]
})
export class HistoriaComponent implements OnInit {
  miesiace = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Luty', 'Sierpień', 'Wrzesień', 'Pażdziernik', 'Listopad', 'Grudzień'];
  wybor: number = null;
  obecnyMiesiac = new Date().getMonth();
  wybranymiesiac = 0;

  constructor(public historiaService: HistoriaService) {
    this.historiaService.historiaLadowanie(this.wybranymiesiac);
  }

  ngOnInit(): void {
  }


  odswierzListe() {
    this.historiaService.historiaLadowanie(this.wybranymiesiac);
  }

  obcMiesiac(i: any) {
    this.wybranymiesiac = +i + 1;
    this.historiaService.historiaLadowanie(this.wybranymiesiac);

  }

  pobierz(transakcja: any) {
    this.historiaService.faktura(transakcja);
  }

  transakcjaClick(i: number) {
    if (this.wybor != i) {
      this.wybor = i;
    } else if (this.wybor === i) {
      this.wybor = null;
    }
  }

}
