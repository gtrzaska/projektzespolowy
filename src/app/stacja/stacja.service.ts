import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import swal from "sweetalert";

@Injectable({
  providedIn: 'root'
})
export class StacjaService implements OnInit {
  link = 'https://server439815.nazwa.pl/PZesp/';
  isLoading = false;
  data = [];
  kody = [];

  constructor(private http: HttpClient) {
    this.kod_sprawdz()
  }

  zakup(karta: string, ilosc: number, cena: number, data: any, id_cennik: number, id_uzytkownik: number, kod: string, produkt: string) {
    this.http.post<any>(this.link + 'transakcje.php', {
      karta, ilosc, cena, data, id_cennik, id_uzytkownik, kod, produkt
    })
      .subscribe(error => {
        if (error == '200') {
          this.isLoading = false;
        } else {
          swal("Coś nie tak");
          this.isLoading = false;
        }
      }, error => {
        console.log(error);

      });
  }

  kod_sprawdz() {
    this.isLoading = true;
    this.kody = [];
    this.data = [];
    this.http.get(this.link + 'kody_fetch.php').subscribe(data => {
      this.data.push(data);
      for (let i = 0; i < this.data[0].length; i++) {
        this.kody[i] = [];
        this.kody[i] = [this.data[0][i].id, this.data[0][i].kod, this.data[0][i].produkt, this.data[0][i].email];
      }
      this.isLoading = false;
    }, error => console.error(error));
  }

  ngOnInit(): void {
    this.kod_sprawdz()
  }
}
