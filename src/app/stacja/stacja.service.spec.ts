import {TestBed} from '@angular/core/testing';
import {StacjaService} from './stacja.service';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";

describe('StacjaService', () => {
  let service: StacjaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
    service = TestBed.inject(StacjaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
