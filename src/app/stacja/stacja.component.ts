import {Component, OnInit} from '@angular/core';
import {CennikService} from "../cennik/cennik.service";
import {NgForm} from "@angular/forms";
import swal from "sweetalert";
import {StacjaService} from "./stacja.service";
import {KlienciService} from "../klienci/klienci.service";

@Component({
  selector: 'app-stacja',
  templateUrl: './stacja.component.html',
  styleUrls: ['./stacja.component.css']
})
export class StacjaComponent implements OnInit {
  private cena: number;
  kod = null;
  cena_calosc = 0;
  promocja = 0;
  private produkt: string = '';

  constructor(public cennikService: CennikService, public stacjaService: StacjaService, private klienciService: KlienciService) {
  }

  ngOnInit(): void {
  }

  onSelect(i: any) {
    console.log(this.cennikService.cennik[i]);
    this.cena = +this.cennikService.cennik[i][2];
    this.produkt = this.cennikService.cennik[i][1];
    console.log(this.cennikService.cennik[i][2]);
  }

  cena_oblicz(f: NgForm) {
    if (this.produkt != '' && +f.value.ilosc != 0) {
      this.cena_calosc = (this.cena * +f.value.ilosc) - (this.promocja * this.cena);
    }
  }

  kod_sprawdz(f: NgForm) {
    this.promocja = 0;
    this.kod = null;
    if (f.value.kod != '' && this.produkt != '' && f.value.email != '') {
      for (let i = 0; i < this.stacjaService.kody.length; i++) {
        if (this.stacjaService.kody[i][1] == f.value.kod && this.stacjaService.kody[i][2] == this.produkt && this.stacjaService.kody[i][3] == f.value.email) {
          this.promocja = 1;
          this.kod = f.value.kod;
          swal('Dodano kod');
          break;
        }
      }
      this.cena_oblicz(f);
    }

  }

  onSubmit(f: NgForm) {
    swal({
      title: "Czy na pewno?",
      text: 'Cena: ' + this.cena_calosc + 'zł',
      icon: "warning",
      buttons: ["Nie", "Tak"],

    })
      .then(potwierdz => {
        if (potwierdz) {
          let id = 0;
          swal("Sukces!", "Zakupiono towar!", "success");
          if (f.value.email != '') {
            for (let i = 0; i < this.klienciService.users.length; i++) {
              if (this.klienciService.users[i][3] == f.value.email) {
                id = this.klienciService.users[i][0];
                break;
              }
            }
          }
          const rok = new Date().getFullYear();
          const miesiac = new Date().getMonth();
          const dzien = new Date().getDate() + 1;
          const obecnadata = new Date(rok, miesiac, dzien);
          let cart = "";
          while (cart.length < 15) {
            cart += Math.floor(Math.random() * 10);
          }
          Math.floor(Math.random() * 10);

          this.stacjaService.zakup(cart, f.value.ilosc, this.cena_calosc, obecnadata, this.cennikService.cennik[f.value.produkt][0], id, this.kod, this.produkt);


        }
      });
  }

}
