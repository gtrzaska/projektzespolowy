import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StacjaComponent} from './stacja.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";

describe('StacjaComponent', () => {
  let component: StacjaComponent;
  let fixture: ComponentFixture<StacjaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StacjaComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StacjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
