import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";
import {CennikComponent} from "./cennik.component";
import {CennikService} from "./cennik.service";

describe('CennikComponent', () => {
  let component: CennikComponent;
  let fixture: ComponentFixture<CennikComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [CennikComponent],
        imports: [
          RouterTestingModule,
          HttpClientModule,
          ReactiveFormsModule,
          FormsModule
        ],
        providers: [
          CennikService,
        ]
      }
    )
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CennikComponent);
    component = fixture.componentInstance;
    let someServiceApi = fixture.debugElement.injector.get(CennikService);
    spyOn(someServiceApi, 'cennikFetch');
    let cennikService = fixture.debugElement.injector.get(CennikService);
    cennikService.isLoading = false;
    cennikService.cennik = [["1", "Benzyna E95", "5.72"],
      ["2", "Benzyna E98", "5.98"],
      ["3", "Olej Napedowy", "5.57"],
      ["4", "LPG", "2.88"],
      ["5", "Mycie standardowe", "500"],
      ["6", "Mycie z woskowaniem", "1500"],
      ["7", "Tyskie", "3.02"],
      ["8", "Red Bull", "4.98"]];
    fixture.detectChanges();
  });

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should create 8 inputs', async(async () => {
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('input'));
    expect(input.length).toEqual(8);
  }));

  it('should add and delete product', async(async () => {
    let cennikService = fixture.debugElement.injector.get(CennikService);
    await cennikService.dodajProdukt('test', '11');
    fixture.detectChanges();
    component.usunProdukt(0);
    fixture.detectChanges();
    expect(component).toBeTruthy();
  }));
});
