import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthComponent} from "./auth.component";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";
import {FormsModule} from '@angular/forms';
import {AuthService} from "./auth.service";

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuthComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should create 2 inputs when logging in', async(async () => {
    let input = fixture.debugElement.queryAll(By.css('.form-group'));
    expect(input.length).toEqual(2);
  }));

  it('should create 7 inputs during registration ', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('.form-group'));
    expect(input.length).toEqual(7);
  }));

  it('should create \'imie\' input', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#imie')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'nazwisko\' input', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#nazwisko')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'Email\' input', async(async () => {
    let input = fixture.debugElement.query(By.css('#email')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'Hasło\' input', async(async () => {
    let input = fixture.debugElement.query(By.css('#password')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'Ulica\' input', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#ulica')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'Miasto\' input', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#miasto')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'Kod-Pocztowy\' input', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#kodPocztowy')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'PESEL\' input', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#pesel')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'REGON\' input', async(async () => {
    component.tryb = 'rejestracja';
    component.czyFirma = true;
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#regon')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should create \'NIP\' input', async(async () => {
    component.tryb = 'rejestracja';
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('#nip')).nativeElement;
    expect(input).toBeTruthy();
  }));

  it('should zip-code validation work correctly', async(async () => {
    let correctZipCode = ['11-222', '35-221'];
    for (let i = 0; i < correctZipCode.length; i++) {
      let isCorrectZipCode = component.kodPocztowyV(correctZipCode[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectZipCode = ['11-22', '5-221', '2222222222', 'sds', '', '4444-34', '33-44444'];
    for (let i = 0; i < correctZipCode.length; i++) {
      let isCorrectZipCode = component.kodPocztowyV(uncorrectZipCode[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

  it('should PESEL validation work correctly', async(async () => {
    let correctPesel = ['97121200934', '90011103616'];
    for (let i = 0; i < correctPesel.length; i++) {
      let isCorrectZipCode = component.peselV(correctPesel[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectPesel = ['11-22', '3333333333333333333333333333333', '33333333', 'sds', ''];
    for (let i = 0; i < uncorrectPesel.length; i++) {
      let isCorrectZipCode = component.peselV(uncorrectPesel[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

  it('should NIP validation work correctly', async(async () => {
    let correctNip = ['1234567890'];
    for (let i = 0; i < correctNip.length; i++) {
      let isCorrectZipCode = component.nipV(correctNip[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectNip = ['11-22', '3333333333333333333333333333333', '', 'sds', '', '3333'];
    for (let i = 0; i < uncorrectNip.length; i++) {
      let isCorrectZipCode = component.nipV(uncorrectNip[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

  it('should REGON validation work correctly', async(async () => {
    let correctRegon = ['123456789'];
    for (let i = 0; i < correctRegon.length; i++) {
      let isCorrectZipCode = component.regonV(correctRegon[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectRegon = ['11-22', '3333333333333333333333333333333', '', 'sds', '', '3333'];
    for (let i = 0; i < uncorrectRegon.length; i++) {
      let isCorrectZipCode = component.regonV(uncorrectRegon[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

  it('should onFirma work correctly', async(async () => {
    component.czyFirma = false;
    fixture.detectChanges();
    component.onFirma();
    expect(component.czyFirma).toEqual(true);
    expect(component.pesel).toEqual(true);
    expect(component.regon).toEqual(false);
  }));

  it('should login', async(async () => {
    component.tryb = 'logowanie';
    let email;
    let authService = fixture.debugElement.injector.get(AuthService);
    await authService.login('jan.kowalski@gmail.com', '123456');
    fixture.detectChanges();
    await authService.user.subscribe(user => {
      let isAuth = !user ? false : true;
      if (isAuth) {
        email = user.email;
      }
    });
    expect(authService.email).toEqual('jan.kowalski@gmail.com');
  }));

});
