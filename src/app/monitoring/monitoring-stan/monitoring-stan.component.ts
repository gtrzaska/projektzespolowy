import {Component, OnInit} from '@angular/core';
import {MonitoringService} from "../monitoring.service";

@Component({
  selector: 'app-monitoring-stan',
  templateUrl: './monitoring-stan.component.html',
  styleUrls: ['./monitoring-stan.component.css']
})
export class MonitoringStanComponent implements OnInit {
  public stan = 1;

  constructor(public monitoringService: MonitoringService) {

  }

  ngOnInit(): void {
    this.monitoringService.stanFetch();

  }

  procentPaliwa(zbiornik) {
    this.stan = zbiornik;
  }

}
