import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MonitoringStanComponent} from './monitoring-stan.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";
import {MonitoringService} from "../monitoring.service";

describe('MonitoringStanComponent', () => {
  let component: MonitoringStanComponent;
  let fixture: ComponentFixture<MonitoringStanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MonitoringStanComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringStanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create 5 buttons', async () => {
    // component.monitoringService.isLoading = false;
    let monitoringService = fixture.debugElement.injector.get(MonitoringService);
    monitoringService.isLoading = false;
    monitoringService.stan = [[1, '1', '1', '1', '1'], [2, '1', '1', '1', '1']];
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('.btn'));
    expect(input.length).toEqual(5);
  });

  it('should change fuel tank', () => {
    component.procentPaliwa(2);
    fixture.detectChanges();
    expect(component.stan).toEqual(2);
  });

});
