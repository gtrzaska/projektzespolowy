import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MonitoringKameryComponent} from './monitoring-kamery.component';
import {By} from "@angular/platform-browser";

describe('MonitoringKameryComponent', () => {
  let component: MonitoringKameryComponent;
  let fixture: ComponentFixture<MonitoringKameryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MonitoringKameryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringKameryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create 3 buttons', () => {
    let input = fixture.debugElement.queryAll(By.css('.btn'));
    expect(input.length).toEqual(3);
  });

  it('should open camera', () => {
    component.zmianaKamery(1);
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css('iframe'));
    expect(input).toBeTruthy();
  });
});
