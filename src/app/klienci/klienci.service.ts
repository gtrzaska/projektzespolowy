import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import swal from "sweetalert";

@Injectable({providedIn: 'root'})
export class KlienciService implements OnInit {
  isFetch = false;
  data = [];
  users = [];
  isLoading = false;
  link = 'https://server439815.nazwa.pl/PZesp/';

  constructor(private http: HttpClient, private router: Router) {
    this.isLoading = true;
    this.http.get(this.link + 'klienci_fetch.php').subscribe(data => {
      this.data.push(data);
      for (let i = 0; i < this.data[0].length; i++) {
        this.users[i] = [];
        this.users[i] = [this.data[0][i].Uzytkownik_id, this.data[0][i].Imie, this.data[0][i].Nazwisko, this.data[0][i].Email, this.data[0][i].Ulica, this.data[0][i].Miasto, this.data[0][i].Kod_pocztowy, this.data[0][i].PESEL, this.data[0][i].REGON, this.data[0][i].NIP];
      }
      this.isFetch = true;
      this.isLoading = false;
    }, error => console.error(error));


  }

  ngOnInit() {
  }

  klienci() {
    this.isLoading = true;
    this.users = [];
    this.data = [];
    if (this.isFetch) {
      this.http.get(this.link + 'klienci_fetch.php').subscribe(data => {
        this.data.push(data);
        for (let i = 0; i < this.data[0].length; i++) {
          this.users[i] = [];
          this.users[i] = [this.data[0][i].Uzytkownik_id, this.data[0][i].Imie, this.data[0][i].Nazwisko, this.data[0][i].Email, this.data[0][i].Ulica, this.data[0][i].Miasto, this.data[0][i].Kod_pocztowy, this.data[0][i].PESEL, this.data[0][i].REGON, this.data[0][i].NIP, this.data[0][i].Haslo];
        }
        this.isLoading = false;
      }, error => console.error(error));
    }
    console.log(this.users);
  }

  usunKlienta(id: number, email: string) {
    let onusun = 'usunKlienta';
    this.http.post<any>(this.link + 'users.php', {
      email,
      onusun,
      id
    })
      .subscribe(error => {
        if (error == '200') {
          console.log(error);
          this.isLoading = false;
          this.klienci();
          this.router.navigate(['./klienci'])
        } else {
          swal("Coś nie tak");
          this.isLoading = false;
        }
      }, error => {

      });
  }

  edytujKlienta(email: string, imie: string, nazwisko: string, ulica: string, miasto: string, kod: string, pesel: string, regon: string, nip: string, haslo: string) {
    let onusun = 'edytujKlienta';
    this.http.post<any>(this.link + 'users.php', {
      email, onusun, imie, nazwisko, ulica, miasto, kod, pesel, regon, nip, haslo
    })
      .subscribe(error => {
        if (error == '200') {
          this.isLoading = false;
          this.klienci();
          this.router.navigate(['./klienci'])
        } else {
          swal("Coś nie tak");
          this.isLoading = false;
        }
      }, error => {

      });
  }


}
