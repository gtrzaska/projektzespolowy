import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KlienciListaComponent} from './klienci-lista.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {KlienciService} from "../klienci.service";
import {By} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";

describe('KlienciListaComponent', () => {
  let component: KlienciListaComponent;
  let fixture: ComponentFixture<KlienciListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KlienciListaComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlienciListaComponent);
    component = fixture.componentInstance;
    let klienciService = fixture.debugElement.injector.get(KlienciService);
    klienciService.isLoading = false;
    klienciService.users = [["41", "Czak", "Noris", "NorisOfiszjal@gmail.com", "Qwerty", "gffgpp", "78-781", "45454545455", "", "4444444444", ""],
      ["22", "Bruce", "Lee", "blee@luj.com", "Toschiba", "Jakotako", "89-111", "", "121211212", "4444444444", "123456"]];
    fixture.detectChanges();
  });

  it('should create', () => {
    component.odswierzListe();
    expect(component).toBeTruthy();
  });

  it('should display 2 buttons', () => {
    fixture.detectChanges();
    let button = fixture.debugElement.queryAll(By.css('button'));
    expect(button.length).toEqual(2);
  });

  it('should display 2 users', () => {
    fixture.detectChanges();
    let button = fixture.debugElement.queryAll(By.css('app-klient'));
    expect(button.length).toEqual(2);
  });

  it('should refresh after click on button ', () => {
    spyOn(component, 'odswierzListe');
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(component.odswierzListe).toHaveBeenCalled();
  });


});
