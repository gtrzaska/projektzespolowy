import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KlientDaneComponent} from './klient-dane.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {KlienciService} from "../klienci.service";

describe('KlientDaneComponent', () => {
  let component: KlientDaneComponent;
  let fixture: ComponentFixture<KlientDaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KlientDaneComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KlientDaneComponent);
    component = fixture.componentInstance;
    component.id = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should zip-code validation work correctly', async(async () => {
    let correctZipCode = ['11-222', '35-221'];
    for (let i = 0; i < correctZipCode.length; i++) {
      let isCorrectZipCode = component.kodPocztowyV(correctZipCode[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectZipCode = ['11-22', '5-221', '2222222222', 'sds', '', '4444-34', '33-44444'];
    for (let i = 0; i < correctZipCode.length; i++) {
      let isCorrectZipCode = component.kodPocztowyV(uncorrectZipCode[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

  it('should PESEL validation work correctly', async(async () => {
    let correctPesel = ['97121200934', '90011103616'];
    for (let i = 0; i < correctPesel.length; i++) {
      let isCorrectZipCode = component.peselV(correctPesel[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectPesel = ['11-22', '3333333333333333333333333333333', '33333333', 'sds', ''];
    for (let i = 0; i < uncorrectPesel.length; i++) {
      let isCorrectZipCode = component.peselV(uncorrectPesel[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

  it('should NIP validation work correctly', async(async () => {
    let correctNip = ['1234567890'];
    for (let i = 0; i < correctNip.length; i++) {
      let isCorrectZipCode = component.nipV(correctNip[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectNip = ['11-22', '3333333333333333333333333333333', '', 'sds', '', '3333'];
    for (let i = 0; i < uncorrectNip.length; i++) {
      let isCorrectZipCode = component.nipV(uncorrectNip[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

  it('should REGON validation work correctly', async(async () => {
    let correctRegon = ['123456789'];
    for (let i = 0; i < correctRegon.length; i++) {
      let isCorrectZipCode = component.regonV(correctRegon[i]);
      expect(isCorrectZipCode).toBeTruthy();
    }

    let uncorrectRegon = ['11-22', '3333333333333333333333333333333', '', 'sds', '', '3333'];
    for (let i = 0; i < uncorrectRegon.length; i++) {
      let isCorrectZipCode = component.regonV(uncorrectRegon[i]);
      expect(isCorrectZipCode).toBeFalse();
    }
  }));

});
