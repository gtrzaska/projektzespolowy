import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DostawyZamowienieComponent} from './dostawy-zamowienie.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {DostawyService} from "../dostawy.service";

describe('DostawyZamowienieComponent', () => {
  let component: DostawyZamowienieComponent;
  let fixture: ComponentFixture<DostawyZamowienieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DostawyZamowienieComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DostawyZamowienieComponent);
    component = fixture.componentInstance;
    let dostawyService = fixture.debugElement.injector.get(DostawyService);
    dostawyService.zamowienie = [[1, 'test', 1], [2, 'test2', 1]];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add product', () => {
    let dostawyService = fixture.debugElement.injector.get(DostawyService);
    dostawyService.dodajDoZamowienia(3, 'produkt', 4);
    fixture.detectChanges();
    expect(dostawyService.zamowienie.length).toEqual(3);
  });

  it('should add product if it is a duplicate', () => {
    let dostawyService = fixture.debugElement.injector.get(DostawyService);
    dostawyService.dodajDoZamowienia(1, 'test', 4);
    expect(dostawyService.zamowienie.length).toEqual(2);
  });

  it('should order product', () => {
    let dostawyService = fixture.debugElement.injector.get(DostawyService);
    dostawyService.zamow();
    expect(dostawyService.zamowienie.length).toEqual(0);
  });

  it('should delete 1 product', () => {
    let dostawyService = fixture.debugElement.injector.get(DostawyService);
    dostawyService.usun(1);
    fixture.detectChanges();
    expect(dostawyService.zamowienie.length).toEqual(1);
  });

  it('should delete all products', () => {
    let dostawyService = fixture.debugElement.injector.get(DostawyService);
    component.wyczysc();
    fixture.detectChanges();
    expect(dostawyService.zamowienie.length).toEqual(0);
  });
});
