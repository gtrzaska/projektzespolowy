import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DostawyProduktComponent} from './dostawy-produkt.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";
import {CennikService} from "../../cennik/cennik.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

describe('DostawyProduktComponent', () => {
  let component: DostawyProduktComponent;
  let fixture: ComponentFixture<DostawyProduktComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DostawyProduktComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DostawyProduktComponent);
    component = fixture.componentInstance;
    let cennikService = fixture.debugElement.injector.get(CennikService);
    cennikService.isLoading = false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create 2 buttons', async () => {
    let buttons = fixture.debugElement.queryAll(By.css('.btn'));
    expect(buttons.length).toEqual(2);
  });

  it('should create input and select', async () => {
    let input = fixture.debugElement.queryAll(By.css('.form-group'));
    expect(input.length).toEqual(2);
  });
  it('should change quantity', async () => {
    component.iloscPopraw(-1);
    fixture.detectChanges();
    expect(component.ile).toEqual(1);
    component.iloscPopraw(12220);
    fixture.detectChanges();
    expect(component.ile).toEqual(component.max);
  });

  it('should change max value', async () => {
    component.onSelect(5);
    fixture.detectChanges();
    expect(component.max).toEqual(10000);
  });

  it('should set on maximum value', async () => {
    component.max = 100;
    component.iloscMax();
    fixture.detectChanges();
    expect(component.ile).toEqual(100);
  });
});
