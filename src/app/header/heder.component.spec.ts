import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderComponent} from './header.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.stanFetch();
    expect(component).toBeTruthy();
  });

  it('should logout', () => {
    component.logout();
    expect(component.uprawnienia).toEqual(null);
  });

  it('should display 4 links when user is not logged', () => {
    component.isAuth = false;
    fixture.detectChanges();
    let link = fixture.debugElement.queryAll(By.css('a'));
    expect(link.length).toEqual(4);
  });

  it('should display 7 links when user is logged as client', () => {
    component.isAuth = true;
    component.uprawnienia = 0;
    fixture.detectChanges();
    let link = fixture.debugElement.queryAll(By.css('a'));
    expect(link.length).toEqual(7);
  });

  it('should display 8 links when user is logged as employee', () => {
    component.isAuth = true;
    component.uprawnienia = 2;
    fixture.detectChanges();
    let link = fixture.debugElement.queryAll(By.css('a'));
    expect(link.length).toEqual(8);
  });

  it('should display 11 links when user is logged as owner', () => {
    component.isAuth = true;
    component.uprawnienia = 1;
    fixture.detectChanges();
    let link = fixture.debugElement.queryAll(By.css('a'));
    expect(link.length).toEqual(11);
  });
});
