import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProfilComponent} from './profil.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";

describe('ProfilComponent', () => {
  let component: ProfilComponent;
  let fixture: ComponentFixture<ProfilComponent>;
  let originalTimeout;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [ProfilComponent],
        imports: [
          RouterTestingModule,
          HttpClientModule,
          ReactiveFormsModule,
          FormsModule
        ],
      }
    )
  }));

  beforeEach(() => {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    fixture = TestBed.createComponent(ProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should create 6 inputs when user is b2b client ', async(() => {
    component.uprawnienia = 0;
    component.profilService.czyFirma = true;
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('.form-group'));
    expect(input.length).toEqual(6);
  }));

  it('should create 6 inputs when user is b2c client ', async(() => {
    component.uprawnienia = 0;
    component.profilService.czyFirma = false;
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('.form-group'));
    expect(input.length).toEqual(6);
  }));

  it('should create 5 inputs when user is employee ', async(() => {
    component.uprawnienia = 1;
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('.form-group'));
    expect(input.length).toEqual(5);

  }));
});
