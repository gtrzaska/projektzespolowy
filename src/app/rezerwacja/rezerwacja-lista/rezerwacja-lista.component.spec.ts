import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RezerwacjaListaComponent} from './rezerwacja-lista.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {By} from "@angular/platform-browser";
import {CennikService} from "../../cennik/cennik.service";
import {RezerwacjaService} from "../rezerwacja.service";

describe('RezerwacjaListaComponent', () => {
  let component: RezerwacjaListaComponent;
  let fixture: ComponentFixture<RezerwacjaListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RezerwacjaListaComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RezerwacjaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    let rezerwacjaService = fixture.debugElement.injector.get(RezerwacjaService);
    rezerwacjaService.isLoading = false;
    rezerwacjaService.rezerwacjeA = [["1", "2020-05-19", "15", 'email@t.r', 'Jan', 'Kowalski'],
      ["2", "2020-05-19", "8", "email@t.r", 'Adam', 'Nowak']];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display 2 reservations', async(async () => {
    component.moje = false;
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('a'));
    expect(input.length).toEqual(2);
  }));

  it('should crate 1 button', async(async () => {
    component.moje = false;
    fixture.detectChanges();
    let input = fixture.debugElement.queryAll(By.css('.btn'));
    expect(input.length).toEqual(1);
  }));

});
