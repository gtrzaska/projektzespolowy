import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RezerwacjaDodajComponent} from './rezerwacja-dodaj.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {By} from "@angular/platform-browser";

describe('RezerwacjaDodajComponent', () => {
  let component: RezerwacjaDodajComponent;
  let fixture: ComponentFixture<RezerwacjaDodajComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RezerwacjaDodajComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RezerwacjaDodajComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create 3 inputs', (async () => {
    let input = fixture.debugElement.queryAll(By.css('input'));
    expect(input.length).toEqual(3);
  }));

  it('should crate 1 button', async(async () => {
    let input = fixture.debugElement.queryAll(By.css('.btn'));
    expect(input.length).toEqual(1);
  }));
});
