import {Injectable, OnInit} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {HttpClient} from "@angular/common/http";
import swal from "sweetalert";

@Injectable({
  providedIn: 'root'
})
export class RezerwacjaService implements OnInit {
  private isAuth = false;
  isLoading = false;
  public email = "";
  public uprawnienia: number;
  public imie = "";
  public nazwisko = "";
  link = 'https://server439815.nazwa.pl/PZesp/';
  data = [];
  rezerwacjeA = [];

  constructor(private authService: AuthService, private http: HttpClient) {

    this.rezerwacje();
    this.authService.user.subscribe(user => {
      this.isAuth = !user ? false : true;
      if (this.isAuth) {
        this.uprawnienia = +user.uprawnienia;
        if (this.uprawnienia === 0) {
          this.imie = user.imie;
          this.nazwisko = user.nazwisko;
          this.email = user.email
        }
      }
    });
  }

  ngOnInit() {
  }

  rezerwacje(email?: string) {
    const rok = new Date().getFullYear();
    const miesiac = new Date().getMonth();
    const dzien = new Date().getDate();
    const obecnadata = new Date(rok, miesiac, dzien);
    this.isLoading = true;
    this.rezerwacjeA = [];
    this.data = [];
    let tryb = 'rezerwacje';
    this.http.get<any>(this.link + 'rezerwacje_fetch.php',).subscribe(data => {
      this.data.push(data);
      for (let i = 0; i < this.data[0].length; i++) {
        if (obecnadata <= new Date(this.data[0][i].Data)) {
          this.rezerwacjeA.push([this.data[0][i].id, this.data[0][i].Data, this.data[0][i].godzina, this.data[0][i].Email, this.data[0][i].Imie, this.data[0][i].Nazwisko]);
        }
      }
      this.isLoading = false;
    }, error => console.error(error));
  }

  dodajRezerwacje(email: string, godzina: string, data: string) {
    if (this.uprawnienia === 0) {
      email = this.email;
    }
    this.isLoading = true;
    let tryb = 'dodaj';
    this.http.post<any>(this.link + 'rezerwacja.php', {
      email, data, godzina, tryb
    })
      .subscribe(error => {
        if (error == '200') {
          this.rezerwacje();
          this.isLoading = false;
        } else if (error == '204') {
          swal("Brak uzytkownika");
          this.isLoading = false;
        } else if (error == '206') {
          swal("Termin niedostępny");
          this.isLoading = false;
        } else {
          swal("Coś nie tak");
          this.isLoading = false;
        }
      }, error => {

      });
  }

}
