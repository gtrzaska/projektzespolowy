import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PracownicyDaneComponent} from './pracownicy-dane.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {PracownicyService} from "../pracownicy.service";

describe('PracownicyDaneComponent', () => {
  let component: PracownicyDaneComponent;
  let fixture: ComponentFixture<PracownicyDaneComponent>;
  let pracownicyService;
  const user = [1, 'Imie', 'Nazwisko', 'Email', '', '1', 'nazwa'];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PracownicyDaneComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(PracownicyDaneComponent);
    component = fixture.componentInstance;
    pracownicyService = await fixture.debugElement.injector.get(PracownicyService);
    fixture.detectChanges();
  });

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));
});
