import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {PracownicyService} from "../pracownicy.service";

@Component({
  selector: 'app-pracownicy-dane',
  templateUrl: './pracownicy-dane.component.html',
  styleUrls: ['./pracownicy-dane.component.css']
})
export class PracownicyDaneComponent implements OnInit {

  id: number;


  constructor(private route: ActivatedRoute,
              public pracownicyService: PracownicyService,
              private router: Router
  ) {
    pracownicyService.pracownicy();
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params.id;
      }
    );
  }

  usun() {
    this.pracownicyService.usunPracownika(this.pracownicyService.users[this.id][3]);
  }

  edytuj(form: NgForm) {
    this.pracownicyService.edytujPracownika(this.pracownicyService.users[this.id][3], form.value.imie, form.value.nazwisko, form.value.stanowisko, this.pracownicyService.users[this.id][4]);
  }

}
